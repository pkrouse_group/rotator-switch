using UnityEngine;
using TMPro;

public class TextDisplayController : MonoBehaviour
{
    [SerializeField] public TMP_Text text;
    void Start()
    {
        SwitchEventSingleton.Instance.onSwitchChange += PrintDialRotation;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void PrintDialRotation(float newAngle)
    {
        text.text = newAngle.ToString();
    }
}
