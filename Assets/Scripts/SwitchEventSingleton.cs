using System;
using UnityEngine;

public class SwitchEventSingleton : MonoBehaviour
{
    public static int snapRotationIncrement = 30;
    public static SwitchEventSingleton Instance { get; private set; }

    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.
        if (Instance != null && Instance != this)
            Destroy(this);
        else
            Instance = this;
    }

    public event Action<float> onSwitchChange;
    public void SwitchChange(float angle)
    {
        onSwitchChange?.Invoke(angle);
    }
}
