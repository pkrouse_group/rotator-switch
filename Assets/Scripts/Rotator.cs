using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace LevelUP.Dial
{
    public class Rotator : MonoBehaviour
    {
        [SerializeField] Transform linkedDial;
        private int snapRotationAmount;
        // angleTolerance will be set to 80% of rotation amount.
        // This means that once you twist the dial 80% of the way to the next spot, it is accepted as done.
        private float angleTolerance; 
        [SerializeField] private GameObject RighthandModel;
        [SerializeField] private GameObject LefthandModel;
        [SerializeField] bool shouldUseDummyHands;

        private IXRSelectInteractor newInteractor;
        private XRBaseInteractor interactor;
        private float startAngle;
        private bool requiresStartAngle = true;
        private bool shouldGetHandRotation = false;
        private XRGrabInteractable grabInteractor => GetComponent<XRGrabInteractable>();

        void Awake()
        {
            if (RighthandModel)
            {
                RighthandModel.SetActive(false);
            }
        }
        private void Start()
        {
            snapRotationAmount = SwitchEventSingleton.snapRotationIncrement;
            angleTolerance = 0.8f * snapRotationAmount;
        }
        private void OnEnable()
        {
            grabInteractor.selectEntered.AddListener(GrabbedBy);
            grabInteractor.selectExited.AddListener(GrabEnd);
        }
        private void OnDisable()
        {
            grabInteractor.selectEntered.RemoveListener(GrabbedBy);
            grabInteractor.selectExited.RemoveListener(GrabEnd);
        }

        private void GrabEnd(SelectExitEventArgs arg0)
        {
            shouldGetHandRotation = false;
            requiresStartAngle = true;
            HandModelVisibility(false);
        }


        private void GrabbedBy(SelectEnterEventArgs arg0)
        {
            // Replaced deprecated/obsolete call.
            // interactor = GetComponent<XRGrabInteractable>().selectingInteractor;
            interactor = (XRBaseInteractor) GetComponent<XRGrabInteractable>().interactorsSelecting[0];
            interactor.GetComponent<XRDirectInteractor>().hideControllerOnSelect = true;
            shouldGetHandRotation = true;
            startAngle = 0f;

            HandModelVisibility(true);
        }

        private void HandModelVisibility(bool visibilityState)
        {
            if (!shouldUseDummyHands)
                return;
            if (interactor.CompareTag("RightHand"))
                RighthandModel.SetActive(visibilityState);
            else
                LefthandModel.SetActive(visibilityState);
        }

        void Update()
        {
            if (shouldGetHandRotation)
            {
                var rotationAngle = GetInteractorRotation(); //gets the current controller angle
                GetRotationDistance(rotationAngle);
            }
        }

        public float GetInteractorRotation() => interactor.GetComponent<Transform>().eulerAngles.z;

        #region TheMath!
        private void GetRotationDistance(float currentAngle)
        {
            if (!requiresStartAngle)
            {
                var angleDifference = Mathf.Abs(startAngle - currentAngle);

                if (angleDifference > angleTolerance)
                {
                    if (angleDifference > 270f) //checking to see if the user has gone from 0-360 - a very tiny movement but will trigger the angletolerance
                    {
                        float angleCheck;

                        if (startAngle < currentAngle) 
                        {
                            angleCheck = CheckAngle(currentAngle, startAngle);

                            if (angleCheck < angleTolerance)
                                return;
                            else
                            {
                                RotateDialClockwise();
                                startAngle = currentAngle;
                            }
                        }
                        else if (startAngle > currentAngle) 
                        {
                            angleCheck = CheckAngle(currentAngle, startAngle);

                            if (angleCheck < angleTolerance)
                                return;
                            else
                            {
                                RotateDialAntiClockwise();
                                startAngle = currentAngle;
                            }
                        }
                    }
                    else
                    {
                        if (startAngle < currentAngle)
                        {
                            RotateDialAntiClockwise();
                            startAngle = currentAngle;
                        }
                        else if (startAngle > currentAngle)
                        {
                            RotateDialClockwise();
                            startAngle = currentAngle;
                        }
                    }
                }
            }
            else
            {
                requiresStartAngle = false;
                startAngle = currentAngle;
            }
        }
        #endregion

        private float CheckAngle(float currentAngle, float startAngle) => (360f - currentAngle) + startAngle;

        private void RotateDialClockwise()
        {
            float newZ = Mathf.Round(linkedDial.localEulerAngles.z + snapRotationAmount);
            newZ = Normalize(newZ);
            linkedDial.localEulerAngles = new Vector3(linkedDial.localEulerAngles.x, 
                                                      linkedDial.localEulerAngles.y,
                                                      newZ);
            // Generate an event
            SwitchEventSingleton.Instance.SwitchChange(newZ);
        }

        private void RotateDialAntiClockwise()
        {
            float newZ = Mathf.Round(linkedDial.localEulerAngles.z - snapRotationAmount);
            if (newZ < 0) newZ += 360f; // isolated to -30 if left from top
                
            linkedDial.localEulerAngles = new Vector3(linkedDial.localEulerAngles.x, 
                                                      linkedDial.localEulerAngles.y,
                                                      newZ);
            // Generate an event
            SwitchEventSingleton.Instance.SwitchChange(newZ);
        }

        private float Normalize(float val)
        {
            if (val == 360f)
                return 0f;
            return val;
        }
    }
}
