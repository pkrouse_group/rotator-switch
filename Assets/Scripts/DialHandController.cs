using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialHandController : MonoBehaviour
{
    private float angle = 0f;
    private bool rotating = false;
    // Queue up the rotates, because dial movement is not as fast as hand movement on the switch.
    Queue<float> myRotationQueue = new Queue<float>();

    private void Start()
    {
        SwitchEventSingleton.Instance.onSwitchChange += RegisterDialRotation;
    }

    private void Update()
    {
        if (myRotationQueue.Count > 0 && !rotating)
        {
            float degrees = myRotationQueue.Dequeue();
            StartCoroutine(RotateZBy(degrees));
        }
    }
    private void RegisterDialRotation(float newAngle)
    {
        int mul = GetMultiplier(newAngle);
        float degrees = 30f * mul;
        myRotationQueue.Enqueue(degrees);
        angle = newAngle;
    }

    private IEnumerator RotateZBy(float degrees)
    {
        float lerpDuration = 0.2f;
        rotating = true;
        float timeElapsed = 0;
        Quaternion startRotation = transform.rotation;
        Quaternion targetRotation = transform.rotation * Quaternion.Euler(0f, 0f, degrees);
        while (timeElapsed < lerpDuration)
        {
            transform.rotation = Quaternion.Slerp(startRotation, targetRotation, timeElapsed / lerpDuration);
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        transform.rotation = targetRotation;
        rotating = false;
    }

    // Sets rotation left or right (negative or positive)
    // based on what user did.
    private int GetMultiplier(float newAngle)
    {
        if (angle == 0f && newAngle == 330f)
            return -1;
        if (angle == 330f && newAngle == 0)
            return 1;
        if (newAngle < angle)
            return -1;
        else
            return 1;
    }
}
